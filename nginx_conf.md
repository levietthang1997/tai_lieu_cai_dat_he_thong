```editorconfig
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
worker_connections 1024;
}


http {
log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
'$status $body_bytes_sent "$http_referer" '
'"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;
	
	upstream jenkins {
		keepalive 32; # keepalive connections
		server 127.0.0.1:8080; # jenkins ip and port
	}

# Required for Jenkins websocket agents
	map $http_upgrade $connection_upgrade {
	  default upgrade;
	  '' close;
	}

    server {
        server_name  kttk.xyz www.kttk.xyz;
        location / {
			root   /usr/share/nginx/html;
			try_files $uri $uri/ /index.html;
			index  index.html index.htm;
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    
    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot








}

	server {
        server_name  api.kttk.xyz;
        location / {
			proxy_pass http://localhost:9999;
			
			# Required for Jenkins websocket agents
			proxy_set_header   Connection        $connection_upgrade;
			proxy_set_header   Upgrade           $http_upgrade;

			proxy_set_header   Host              $host;
			proxy_set_header   X-Real-IP         $remote_addr;
			proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
			proxy_set_header   X-Forwarded-Proto $scheme;
			proxy_max_temp_file_size 0;

			#this is the maximum upload size
			client_max_body_size       8192m;
			client_body_buffer_size    1024m;

			proxy_connect_timeout      90;
			proxy_send_timeout         90;
			proxy_read_timeout         90;
			proxy_buffering            off;
			proxy_request_buffering    off; # Required for HTTP CLI commands
			proxy_set_header Connection ""; # Clear for keepalive
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_buffer_size   12m;
			proxy_buffers   8 2m;
			proxy_busy_buffers_size   12m;
			fastcgi_buffers 16 12m; 
			fastcgi_buffer_size 12m;
			proxy_headers_hash_max_size 4096;
			proxy_headers_hash_bucket_size 1024;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
    
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot




}

	server {
        server_name  elastic.kttk.xyz;
        location / {
			proxy_pass http://localhost:9200;
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
    
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot




}


	server {
        server_name  jenkins.kttk.xyz;
        location / {
			  sendfile off;
			  proxy_pass         http://jenkins;
			  proxy_redirect     default;
			  proxy_http_version 1.1;

			  # Required for Jenkins websocket agents
			  proxy_set_header   Connection        $connection_upgrade;
			  proxy_set_header   Upgrade           $http_upgrade;

			  proxy_set_header   Host              $host;
			  proxy_set_header   X-Real-IP         $remote_addr;
			  proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
			  proxy_set_header   X-Forwarded-Proto $scheme;
			  proxy_max_temp_file_size 0;

			  #this is the maximum upload size
			  client_max_body_size       8192m;
			  client_body_buffer_size    1024m;

			  proxy_connect_timeout      90;
			  proxy_send_timeout         90;
			  proxy_read_timeout         90;
			  proxy_buffering            off;
			  proxy_request_buffering    off; # Required for HTTP CLI commands
			  proxy_set_header Connection ""; # Clear for keepalive
			  proxy_set_header X-Forwarded-Host $host;   
			  proxy_set_header X-Forwarded-Proto https;
			  proxy_set_header X-Forwarded-Server $host;  
			  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			  proxy_buffer_size   12m;
			  proxy_buffers   8 2m;
			  proxy_busy_buffers_size   12m;
			  fastcgi_buffers 16 12m; 
			  fastcgi_buffer_size 12m;
			  proxy_headers_hash_max_size 4096;
			  proxy_headers_hash_bucket_size 1024;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
    
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot




}

	server {
        server_name  nexus.kttk.xyz;
        location / {
			proxy_pass http://localhost:9003;
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			client_max_body_size       8192m;
			client_body_buffer_size    1024m;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
    
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot




}

	server {
        server_name  portainer.kttk.xyz;
        location / {
			proxy_pass http://localhost:9002;
			proxy_set_header   Connection        $connection_upgrade;
			proxy_set_header   Upgrade           $http_upgrade;
			
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
    
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0001/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0001/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

	server {
        server_name  app.kttk.xyz;
        location / {
			proxy_pass http://localhost:4200;
			proxy_set_header   Connection        $connection_upgrade;
			proxy_set_header   Upgrade           $http_upgrade;
			
			# Required for Jenkins websocket agents
			proxy_set_header   Connection        $connection_upgrade;
			proxy_set_header   Upgrade           $http_upgrade;

			proxy_set_header   Host              $host;
			proxy_set_header   X-Real-IP         $remote_addr;
			proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
			proxy_set_header   X-Forwarded-Proto $scheme;
			proxy_max_temp_file_size 0;

			#this is the maximum upload size
			client_max_body_size       8192m;
			client_body_buffer_size    1024m;

			proxy_connect_timeout      90;
			proxy_send_timeout         90;
			proxy_read_timeout         90;
			proxy_buffering            off;
			proxy_request_buffering    off; # Required for HTTP CLI commands
			proxy_set_header Connection ""; # Clear for keepalive
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_buffer_size   12m;
			proxy_buffers   8 2m;
			proxy_busy_buffers_size   12m;
			fastcgi_buffers 16 12m; 
			fastcgi_buffer_size 12m;
			proxy_headers_hash_max_size 4096;
			proxy_headers_hash_bucket_size 1024;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;



    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


}

	server {
        server_name  sso.kttk.xyz;
        location / {
			proxy_pass http://localhost:7777;
			proxy_set_header   Connection        $connection_upgrade;
			proxy_set_header   Upgrade           $http_upgrade;
			proxy_set_header Host $host;
			proxy_buffer_size   12m;
			proxy_buffers   8 2m;
			proxy_busy_buffers_size   12m;
			fastcgi_buffers 16 12m; 
			fastcgi_buffer_size 12m;
			proxy_set_header X-Forwarded-Host $host;   
			proxy_set_header X-Forwarded-Proto https;
			proxy_set_header X-Forwarded-Server $host;  
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_headers_hash_max_size 4096;
			proxy_headers_hash_bucket_size 1024;
		}

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;



    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/kttk.xyz-0002/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/kttk.xyz-0002/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2;
#        listen       [::]:443 ssl http2;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers HIGH:!aNULL:!MD5;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }



    server {
    if ($host = www.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  kttk.xyz www.kttk.xyz;
    return 404; # managed by Certbot




}

	server {
    if ($host = api.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  api.kttk.xyz;
    return 404; # managed by Certbot


}


	server {
    if ($host = jenkins.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  jenkins.kttk.xyz;
    return 404; # managed by Certbot
}

	server {
    if ($host = elastic.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  elastic.kttk.xyz;
    return 404; # managed by Certbot


}


	server {
    if ($host = portainer.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  portainer.kttk.xyz;
    return 404; # managed by Certbot
}
server {
if ($host = nexus.kttk.xyz) {
return 301 https://$host$request_uri;
} # managed by Certbot


        listen       80;
        listen       [::]:80;
        server_name  nexus.kttk.xyz;
    return 404; # managed by Certbot


}


	server {
    if ($host = app.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        server_name  app.kttk.xyz;
    listen 80;
    return 404; # managed by Certbot


}

	server {
	proxy_buffering off;
    if ($host = sso.kttk.xyz) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        server_name  sso.kttk.xyz;
    listen 80;
    return 404; # managed by Certbot


}}
```
