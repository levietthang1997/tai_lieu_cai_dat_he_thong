# Hướng dẫn Cài đặt Rancher với Nginx Reverse Proxy

## Mục lục
- [1. Yêu cầu hệ thống](#1-yêu-cầu-hệ-thống)
- [2. Cài đặt Docker](#2-cài-đặt-docker)
- [3. Cài đặt Rancher](#3-cài-đặt-rancher)
- [4. Cài đặt và cấu hình Nginx](#4-cài-đặt-và-cấu-hình-nginx)
- [5. Cấu hình SSL với Let's Encrypt](#5-cấu-hình-ssl-với-lets-encrypt)
- [6. Kiểm tra và khắc phục sự cố](#6-kiểm-tra-và-khắc-phục-sự-cố)

## 1. Yêu cầu hệ thống
- Ubuntu 20.04 LTS trở lên
- RAM: tối thiểu 4GB
- CPU: 2 cores trở lên
- Đã trỏ DNS cho domain chính (test.com) và subdomain (rancher.test.com)
- Port 80 và 443 được mở trên firewall

## 2. Cài đặt Docker
```bash
# Cập nhật package index
sudo apt update

# Cài đặt các package cần thiết
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Thêm Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Thêm Docker repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Cài đặt Docker
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io

# Khởi động Docker
sudo systemctl start docker
sudo systemctl enable docker

# Kiểm tra Docker đã cài đặt thành công
docker --version
```

## 3. Cài đặt Rancher
```bash
docker run -d \
  --restart=on-failure:5 \
  -p 8080:80 -p 8443:443 \
  --security-opt=no-new-privileges \
  rancher/rancher:latest
```

## 4. Cài đặt và cấu hình Nginx
### Cài đặt Nginx
```bash
sudo apt update
sudo apt install -y nginx
```

### Tạo cấu hình Nginx
Tạo file `/etc/nginx/conf.d/virtual.conf` với nội dung sau:

```nginx
upstream rancher-server {
    server 127.0.0.1:8443;
}

# Server block cho web chính test.com
server {
    listen 80;
    server_name test.com;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    server_name test.com;

    ssl_certificate /etc/letsencrypt/live/test.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/test.com/privkey.pem;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers HIGH:!aNULL:!MD5;
    
    root /var/www/test.com/html;
    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

# Server block cho rancher.test.com
server {
    listen 80;
    server_name rancher.test.com;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    server_name rancher.test.com;

    ssl_certificate /etc/letsencrypt/live/rancher.test.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/rancher.test.com/privkey.pem;
    
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers HIGH:!aNULL:!MD5;
    
    location / {
        proxy_pass https://rancher-server;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        
        # Cấu hình WebSocket
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_http_version 1.1;
        
        # Header bảo mật bổ sung
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Host $host;

        # Timeout settings
        proxy_read_timeout 900s;
        proxy_connect_timeout 900s;
        proxy_send_timeout 900s;
        
        # Tắt buffering
        proxy_buffering off;
        proxy_request_buffering off;
    }
}
```

## 5. Cấu hình SSL với Let's Encrypt
### Cài đặt Certbot
```bash
sudo apt install -y certbot python3-certbot-nginx
```

### Lấy SSL certificates
```bash
# Cho domain chính
sudo certbot --nginx -d test.com

# Cho subdomain Rancher
sudo certbot --nginx -d rancher.test.com
```

### Kiểm tra auto-renewal
```bash
sudo certbot renew --dry-run
```

## 6. Kiểm tra và khắc phục sự cố

### Kiểm tra cấu hình Nginx
```bash
sudo nginx -t
```

### Khởi động lại Nginx
```bash
sudo nginx -s reload
```

### Kiểm tra logs
```bash
# Xem logs Nginx
sudo tail -f /var/log/nginx/error.log
sudo tail -f /var/log/nginx/access.log

# Xem logs Docker
docker logs <container_id>
```

### Một số lỗi thường gặp

1. **Error establishing a database connection**
   - Kiểm tra container Rancher đã chạy chưa
   - Kiểm tra ports 8080 và 8443 đã được mở

2. **SSL certificate errors**
   - Đảm bảo certificates đã được tạo đúng
   - Kiểm tra đường dẫn certificates trong cấu hình Nginx

3. **WebSocket connection errors**
   - Kiểm tra cấu hình WebSocket trong Nginx
   - Đảm bảo proxy headers được set đúng

4. **502 Bad Gateway**
   - Kiểm tra Rancher container đang chạy
   - Kiểm tra upstream configuration

### SELinux (nếu có sử dụng)
```bash
# Cho phép Nginx proxy connections
sudo setsebool -P httpd_can_network_connect 1
```

## Truy cập Rancher
Sau khi hoàn tất cài đặt, bạn có thể truy cập:
- Website chính tại: https://test.com
- Rancher dashboard tại: https://rancher.test.com

Mật khẩu admin mặc định sẽ được hiển thị trong logs của container Rancher:
```bash
docker logs <container_id> 2>&1 | grep "Bootstrap Password:"
```

---
**Lưu ý**: Đảm bảo thay thế `test.com` và `rancher.test.com` bằng domain thực tế của bạn trong toàn bộ hướng dẫn này.