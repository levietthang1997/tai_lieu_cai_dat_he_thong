#insert cert ssl nginx
```commandline
sudo certbot --nginx -d kttk.xyz -d www.kttk.xyz -d api.kttk.xyz -d jenkins.kttk.xyz -d elastic.kttk.xyz -d nexus.kttk.xyz -d app.kttk.xyz -d sso.kttk.xyz -d portainer.kttk.xyz -d agent-portainer.kttk.xyz
```
