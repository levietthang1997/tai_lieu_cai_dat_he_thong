# Cách cài đặt metrics server của k8s trên centos 7
### Để Rancher có thể monitor ram, cpu của các pods, ta phải cài metrics server ở node MASTER
### Lưu ý tất cả các lệnh cài đều chạy bằng root và chạy ở node MASTER và phải có 2 nodes worker -> tổng có 3 nodes
*
* Vì Kubernetes có nhiều port nên để thuận tiện thì disable hết firewall
```shell
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/high-availability-1.21+.yaml
 ```
* Disable ssl verify:
```shell
kubectl patch deployment metrics-server -n kube-system --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]'
```
* Enable hostNetwork
```shell
kubectl edit deployments.apps -n kube-system metrics-server
```
* Thêm vào dòng sau ở config:
hostNetwork: true

![](images/metrics_server1.png)