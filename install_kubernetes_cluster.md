# Cách cài đặt kubernetes trên centos 7
### Kubernetes cần có <strong>1</strong> node <strong>Master</strong> và <strong>n</strong> nodes slave nên cần 1 vm để cài master và 2 hoặc nhiều hơn vm để cài slave
### Lưu ý tất cả các lệnh cài đều chạy bằng root
* Cài docker, wget, vim (đã có tài liệu cài)
* Vì Kubernetes có nhiều port nên để thuận tiện thì disable hết firewall
```shell
systemctl disable firewalld; systemctl stop firewalld
 ```
* Disable swap:
```shell
swapoff -a; sed -i '/swap/d' /etc/fstab
```
* Disable SELinux
```shell
setenforce 0
sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux
```
* Update sysctl settings for Kubernetes networking:
```shell
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
```
* Add yum repo:
```shell
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```
* Install Kubernetes components:
```shell
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
```
* Init service:

```shell
sudo systemctl enable --now kubelet
```
Sau đó reload lại service bằng lệnh:
```shell
sudo systemctl daemon-reload
```

### Bước tiếp theo là cài đặt master và slave

* Để chạy được kubeadm mà không gặp lỗi x509 ở cả master và các worker, phải thực hiện các thao tác sau:
```shell
$rm -rf /etc/containerd/config.toml
$mkdir -p /etc/containerd
$containerd config default > /etc/containerd/config.toml
$systemctl restart containerd
$systemctl enable containerd
$systemctl restart kubelet
```
### - Đối với master node:
* Khởi chạy api server:
```shell
kubeadm init --apiserver-advertise-address=172.16.16.100 --pod-network-cidr=192.168.0.0/16
```
Trong đó 172.16.16.100 là IP address của máy ảo, còn pod network kia thì giữ nguyên
* Fix lỗi x509 ở master node thì dùng lệnh sau
```shell
$ sudo rm -rf $HOME/.kube
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
$ sudo systemctl restart kubelet
```
* Setup calico network (Nên nhớ là cài bản mới nhất nếu không bị conflict với rancher và k8s)
```shell
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.1/manifests/tigera-operator.yaml

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.1/manifests/custom-resources.yaml

kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.1/manifests/calicoctl-etcd.yaml
```
* Tạo lệnh join cho slave kết nối tới master (*):
```shell
kubeadm token create --print-join-command
```
* Sử dụng lệnh sau để check nodes cluster:
```shell
kubectl get nodes
```
Lưu ý rằng timezone và date của các node phải giống y hệt nhau. Ta phai set ve utc+7
```shell
sudo timedatectl set-timezone Asia/Ho_Chi_Minh
```

### - Đối với slave:
* Sử dụng url join được generate ở bước (*) để chạy -> done
